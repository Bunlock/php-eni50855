<?php
require_once 'Moteur.class.php';

class Voiture{
    const NB_ROUES = 4;

    private static $nb_voitures;

    public static function getNbVoitures(){
        return self::$nb_voitures;
    }

    private $immatriculation;
    private $kilometrage;
    private $carburant;
    private $moteur;

    public function __construct($immat, $carbu, $km, $puissance){
        $this->immatriculation = $immat;
        $this->carburant = $carbu; // || 0;
        $this->kilometrage = $km; // || 0;  
        $this->moteur = new Moteur($puissance);
        self::$nb_voitures ++;
    }
    
    public function __destruct()
    {
        unset($this->moteur);   
        self::$nb_voitures --;
    }

    public function rouler($distance){
        $this->kilometrage += $distance;
        $this->carburant -= $this->moteur->consommer($distance);
    }

    public function getImmatriculation(){
        return $this->immatriculation;
    }

    public function getKilometrage(){
        return $this->kilometrage;
    }

    public function getCarburant(){
        return $this->carburant;
    }

    public function getPuissance(){
        return $this->moteur->getPuissance();
    }
}

?>