<?php

require_once 'Voiture.class.php';

class Batmobile extends Voiture {
    private static $instance;

    public static function getBatmobile(){
        if (!isset(self::$instance)){
            self::$instance = new Batmobile();
        }
        return self::$instance;
    }

    private function __construct(){
        
    }


}

?>