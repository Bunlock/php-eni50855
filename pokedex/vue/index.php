<html>
	<head>
		<title>Pokedex</title>
		<?php include 'partials/head.php' ?>
	</head>
	<body>	
		<?php include 'partials/header.php' ?>
		<main></main>
		<?php include 'partials/footer.php' ?>
	</body>
</html>