<?php 
	function feedSelectType(){
		$servername = "localhost";
		$username = "root";
		$password = "";

		try {
			$conn = new PDO("mysql:host=$servername;dbname=pokedex", $username, $password);
			// set the PDO error mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			//echo "Connected successfully"; 
			}
		catch(PDOException $e)
			{
			echo "Connection failed: " . $e->getMessage();
			}
			
		
			$types = $conn->query('SELECT * FROM types');
			// solution avec FETCHALL
			$types = $types->fetchAll();
			//var_dump($types);
			
			$flow = '';
			foreach ($types as $i){ 
				$flow .= '<option value="'.$i['id_type'].'">';
				$flow .= $i['libelle'];
				$flow .= '</option>';
			}
			
			return $flow;
	}
	
	
	?>
<html>
	<head>
			<title>Pokedex</title>
			<link rel="stylesheet" href="../bootstrap/bootstrap.min.css" />
			<script src="../bootstrap/bootstrap.min.js"></script>
			<meta charset="UTF-8">
	</head>
	<?php include 'header.php'; ?>

<form>
	<label>Nom <input name="nom"></label>
	<label>Image <input name="img"></label>
	<label>Description <textarea name="description"></textarea></label>
	<label>Type <select name="type">
		<?php echo feedSelectType(); ?>
	</select></label>
</form>
<?php include 'footer.php'; ?>