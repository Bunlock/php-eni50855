<?php 
	require_once 'ctrl/sdb.php';
	
	$conn = SDB::GetInstance();
	$data = $conn->query('SELECT * FROM types');
	//var_dump($data);
		
?>
<html>
	<head>
			<title>Pokedex</title>
			<?php include 'partials/head.php'; ?>
	</head>
	<body>
		<?php include 'partials/header.php'; ?>
	<main>
		<form>
			<label>Nom <input id="nom" name="nom"></label>
			<label>Image <input id="img" name="img"></label>
			<label>Description <textarea id="description" name="description"></textarea></label>
			<label>Type <select id="type" name="type">
				<?php 
					
					//solution avec FETCH
					while ($type = $data->fetch()){ ?>
						<option value="<?php echo $type['id_type'] ?>">
						<?php echo $type['libelle'] ?></option>
						
					<?php }  ?>
			</select></label>
			
		</form>
		<button class="btn btn-success" id="add_pkm">+</button>
	</main>
	<script>
		
		$(()=>{			
			//function(){} equivaut à ()=>{}
			$('#add_pkm').click((event)=>{
				//event.preventDefault();
				let data = {
					nom: $('#nom').val(), 
					img: $('#img').val(), 
					description: $('#description').val(),
					type: $('#type').val()
				}
				//console.log(data);
				
				$.get({
					url: "?action=save_pkm", // /?action...
					data: data, 
					success: (result)=>{
						console.log(result);
						$('main').append('<div class="alert-success">'+result+'</div>');
					
					},
					error: (err)=>{
						console.log(err);
						$('main').append('<div class="alert-danger">'+err+'</div>');
					}
			  });
			})
			
		});
		
	</script>
<?php include 'partials/footer.php'; ?>
	</body>
</html>