DROP TABLE casting;
CREATE TABLE casting (
	ident_film integer,
	ident_acteur integer,
	role varchar(100),
	nb_jour_tournage integer
);

INSERT INTO casting VALUES
	(1, 1, 'Helena', 100);

INSERT INTO casting VALUES
	(1, 2, 'Fred', 100);

INSERT INTO casting VALUES
	(1, 3, 'Insepcteur Gesberg', NULL);
	
INSERT INTO casting VALUES
	(1, 4, 'Le Fleuriste', 35);

INSERT INTO casting VALUES
	(1, 10, 'Le Batteur', 20);

INSERT INTO casting VALUES
	(2, 5, 'Nikita', 68);
	
INSERT INTO casting VALUES
	(2, 10, 'Victor le nettoyeur', 9);
	
INSERT INTO casting VALUES
	(3, 6, 'Han Solo', 201);
	
INSERT INTO casting VALUES
	(3, 7, 'Princesse Leia', 203);
	
INSERT INTO casting VALUES
	(4, 8, 'Neytiri', 50);
	
INSERT INTO casting VALUES
	(4, 9, 'Dr. Grace Augustine', 45);

INSERT INTO casting VALUES
	(5, 11, 'Antoine Bailleul', 125);

INSERT INTO casting VALUES
	(5, 12, 'Philippe Abrams', 126);