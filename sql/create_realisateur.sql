DROP TABLE realisateur;
CREATE TABLE realisateur (
	ident_realisateur integer,
	nom varchar(50),
	prenom varchar(20),
	date_naissance date,
	nb_film_ecrit integer,
	nb_film_produit integer,
	nationalite smallint
);

INSERT INTO realisateur VALUES
	(1, 'Besson', 'Luc', STR_TO_DATE('18/03/1959','%d/%m/%Y'), 40, 99, 1);

INSERT INTO realisateur VALUES
	(2, 'Lucas', 'Georges', STR_TO_DATE('14/05/1944','%d/%m/%Y'), 79, 64, 2);

INSERT INTO realisateur VALUES
	(3, 'Cameron', 'James', STR_TO_DATE('16/08/1954','%d/%m/%Y'), 22, 23, 2);

INSERT INTO realisateur VALUES
	(4, 'Boon', 'Dany', STR_TO_DATE('26/06/1966','%d/%m/%Y'), 5, 1, 1);
