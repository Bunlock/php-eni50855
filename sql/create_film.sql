DROP TABLE film;
CREATE TABLE film (
	ident_film integer,
	titre varchar(50),
	genre1 varchar(20),
	genre2 varchar(20),
	date_sortie date,
	pays smallint,
	ident_realisateur integer,
	distributeur varchar(50),
	resume varchar(2000)
);

INSERT INTO film VALUES
	(1, 'Subway','policier','drame', STR_TO_DATE('10/04/1985','%d/%m/%Y'), 1, 1, 'Gaumont', 'Conte les aventures de la population souterraine dans les couloirs du métro parisien');
	
INSERT INTO film VALUES
	(2, 'Nikita', 'drame', 'romantique', STR_TO_DATE('21/02/1990','%d/%m/%Y'), 1,1, 'Gaumont', 'Nikita condamnée à la prison à perpétuité est contrainte à travailler secrètement pour le gouvernement en tant que aagent hautement qualifié des services secrets');
	
INSERT INTO film VALUES
	(3, 'Star Wars 6 : Le retour du Jedi', 'action', 'science-fiction', STR_TO_DATE('19/10/1983','%d/%m/%Y'), 2,2, '20th Century Fox', 'L\'Empire galactique est plus puissant que jamais : la construction de la nouvelle arme, l\'Etoile de la Mort, menace l\'univers tout entier.');
	
INSERT INTO film VALUES
	(4, 'Avatar', 'action', 'science-fiction', STR_TO_DATE('16/10/2009','%d/%m/%Y'), 2,3, '20th Century Fox', 'Malgré sa paralysie, Jack Sully, un ancien marine immobilisé dans un fauteuil roulant, est resté un combattant au plus profond');
	
INSERT INTO film VALUES
	(5, 'Bienvenue chez les Ch\'tis', 'comédie', '', STR_TO_DATE('27/02/2008','%d/%m/%Y'), 1,4, 'Pathé', 'Philippe Abrams est directeur de la poste de Salon-de-Provence est muté dans le Nord.');
	

	
	


	