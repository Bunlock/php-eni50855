DROP TABLE statistique;
CREATE TABLE statistique (
	ident_film integer,
	durée integer,
	nb_entree_france decimal(15,0),
	recette_usa decimal(15,2),
	recette_monde decimal(15,2),
	budget decimal(12,2)
);

INSERT INTO statistique VALUES
	(1, 104, 2917562, 390659,1272637.45,2.6);
INSERT INTO statistique VALUES
	(2, 118, 3787845, 5017971,0,7.6);
INSERT INTO statistique VALUES
	(3, 133, 4263000, 191648000,472000000,32);
INSERT INTO statistique VALUES
	(4, 170, 12018251, 760505847,2946271769,237);
INSERT INTO statistique VALUES
	(5, 100, 21000000, 0,245000000,11);