DROP TABLE IF EXISTS realisateur;
CREATE TABLE realisateur (
	ident_realisateur integer,
	nom varchar(50),
	prenom varchar(20),
	date_naissance date,
	nb_film_ecrit integer,
	nb_film_produit integer,
	nationalite smallint
);

INSERT INTO realisateur VALUES
	(1, 'Besson', 'Luc', STR_TO_DATE('18/03/1959','%d/%m/%Y'), 40, 99, 1);

INSERT INTO realisateur VALUES
	(2, 'Lucas', 'Georges', STR_TO_DATE('14/05/1944','%d/%m/%Y'), 79, 64, 2);

INSERT INTO realisateur VALUES
	(3, 'Cameron', 'James', STR_TO_DATE('16/08/1954','%d/%m/%Y'), 22, 23, 2);

INSERT INTO realisateur VALUES
	(4, 'Boon', 'Dany', STR_TO_DATE('26/06/1966','%d/%m/%Y'), 5, 1, 1);

	
	DROP TABLE IF EXISTS pays;
CREATE TABLE pays (
	ident_pays smallint,
	libelle varchar(100)
);

INSERT INTO pays VALUES
	(1, 'FRANCE');
INSERT INTO pays VALUES
	(2, 'USA');
INSERT INTO pays VALUES
	(3, 'ALGERIE');
	
DROP TABLE IF EXISTS film;
CREATE TABLE film (
	ident_film integer,
	titre varchar(50),
	genre1 varchar(20),
	genre2 varchar(20),
	date_sortie date,
	pays smallint,
	ident_realisateur integer,
	distributeur varchar(50),
	resume varchar(2000)
);

INSERT INTO film VALUES
	(1, 'Subway','policier','drame', STR_TO_DATE('10/04/1985','%d/%m/%Y'), 1, 1, 'Gaumont', 'Conte les aventures de la population souterraine dans les couloirs du métro parisien');
	
INSERT INTO film VALUES
	(2, 'Nikita', 'drame', 'romantique', STR_TO_DATE('21/02/1990','%d/%m/%Y'), 1,1, 'Gaumont', 'Nikita condamnée à la prison à perpétuité est contrainte à travailler secrètement pour le gouvernement en tant que aagent hautement qualifié des services secrets');
	
INSERT INTO film VALUES
	(3, 'Star Wars 6 : Le retour du Jedi', 'action', 'science-fiction', STR_TO_DATE('19/10/1983','%d/%m/%Y'), 2,2, '20th Century Fox', 'L\'Empire galactique est plus puissant que jamais : la construction de la nouvelle arme, l\'Etoile de la Mort, menace l\'univers tout entier.');
	
INSERT INTO film VALUES
	(4, 'Avatar', 'action', 'science-fiction', STR_TO_DATE('16/10/2009','%d/%m/%Y'), 2,3, '20th Century Fox', 'Malgré sa paralysie, Jack Sully, un ancien marine immobilisé dans un fauteuil roulant, est resté un combattant au plus profond');
	
INSERT INTO film VALUES
	(5, 'Bienvenue chez les Ch\'tis', 'comédie', '', STR_TO_DATE('27/02/2008','%d/%m/%Y'), 1,4, 'Pathé', 'Philippe Abrams est directeur de la poste de Salon-de-Provence est muté dans le Nord.');
	

DROP TABLE IF EXISTS casting;
CREATE TABLE casting (
	ident_film integer,
	ident_acteur integer,
	role varchar(100),
	nb_jour_tournage integer
);

INSERT INTO casting VALUES
	(1, 1, 'Helena', 100);

INSERT INTO casting VALUES
	(1, 2, 'Fred', 100);

INSERT INTO casting VALUES
	(1, 3, 'Insepcteur Gesberg', NULL);
	
INSERT INTO casting VALUES
	(1, 4, 'Le Fleuriste', 35);

INSERT INTO casting VALUES
	(1, 10, 'Le Batteur', 20);

INSERT INTO casting VALUES
	(2, 5, 'Nikita', 68);
	
INSERT INTO casting VALUES
	(2, 10, 'Victor le nettoyeur', 9);
	
INSERT INTO casting VALUES
	(3, 6, 'Han Solo', 201);
	
INSERT INTO casting VALUES
	(3, 7, 'Princesse Leia', 203);
	
INSERT INTO casting VALUES
	(4, 8, 'Neytiri', 50);
	
INSERT INTO casting VALUES
	(4, 9, 'Dr. Grace Augustine', 45);

INSERT INTO casting VALUES
	(5, 11, 'Antoine Bailleul', 125);

INSERT INTO casting VALUES
	(5, 12, 'Philippe Abrams', 126);
	

DROP TABLE IF EXISTS acteur;
CREATE TABLE IF NOT EXISTS acteur (
	ident_acteur integer,
	nom varchar(50),
	prenom varchar(20),
	date_naissance date,
	nb_film integer,
	nationalite smallint
);

INSERT INTO acteur VALUES
	(1, 'Adjani','Isabelle', STR_TO_DATE('27/06/1955','%d/%m/%Y'), 42, 1);
	
INSERT INTO acteur VALUES
	(2, 'Lambert','Christophe', STR_TO_DATE('29/03/1957','%d/%m/%Y'), 64, 1);

INSERT INTO acteur VALUES
	(3, 'Bohringer','Richard', STR_TO_DATE('16/06/1942','%d/%m/%Y'), 132, 1);
	
INSERT INTO acteur VALUES
	(4, 'Galabru','Michel', STR_TO_DATE('27/10/1922','%d/%m/%Y'), 277, 1);

INSERT INTO acteur VALUES
	(5, 'Parillaud','Anne', STR_TO_DATE('06/05/1960','%d/%m/%Y'), 35, 1);
	
INSERT INTO acteur VALUES
	(6, 'Ford','Harrison', STR_TO_DATE('13/06/1942','%d/%m/%Y'), 64, 2);
	
INSERT INTO acteur VALUES
	(7, 'Fisher','Carrie', STR_TO_DATE('21/10/1956','%d/%m/%Y'), 74, 2);
	
INSERT INTO acteur VALUES
	(8, 'Saldana','Zoé', STR_TO_DATE('19/06/1978','%d/%m/%Y'), 31, 2);
	
INSERT INTO acteur VALUES
	(9, 'Weaver','Sigourney', STR_TO_DATE('08/10/1949','%d/%m/%Y'), 66, 2);

INSERT INTO acteur VALUES
	(10, 'Reno','Jean', STR_TO_DATE('30/06/1948','%d/%m/%Y'), 75, 1);
	
INSERT INTO acteur VALUES
	(11, 'Boon','Danny', STR_TO_DATE('26/06/1966','%d/%m/%Y'), 23, 1);
	
INSERT INTO acteur VALUES
	(12, 'Merad','Kad', STR_TO_DATE('27/03/1964','%d/%m/%Y'), 55, 3);
	


DROP TABLE IF EXISTS statistique;
CREATE TABLE statistique (
	ident_film integer,
	durée integer,
	nb_entree_france decimal(15,0),
	recette_usa decimal(15,2),
	recette_monde decimal(15,2),
	budget decimal(12,2)
);

INSERT INTO statistique VALUES
	(1, 104, 2917562, 390659,1272637.45,2.6);
INSERT INTO statistique VALUES
	(2, 118, 3787845, 5017971,0,7.6);
INSERT INTO statistique VALUES
	(3, 133, 4263000, 191648000,472000000,32);
INSERT INTO statistique VALUES
	(4, 170, 12018251, 760505847,2946271769,237);
INSERT INTO statistique VALUES
	(5, 100, 21000000, 0,245000000,11);	
	

	

	

	
	
	

	