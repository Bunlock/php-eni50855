DROP TABLE acteur;
CREATE TABLE acteur (
	ident_acteur integer,
	nom varchar(50),
	prenom varchar(20),
	date_naissance date,
	nb_film integer,
	nationalite smallint
);

INSERT INTO acteur VALUES
	(1, 'Adjani','Isabelle', STR_TO_DATE('27/06/1955','%d/%m/%Y'), 42, 1);
	
INSERT INTO acteur VALUES
	(2, 'Lambert','Christophe', STR_TO_DATE('29/03/1957','%d/%m/%Y'), 64, 1);

INSERT INTO acteur VALUES
	(3, 'Bohringer','Richard', STR_TO_DATE('16/06/1942','%d/%m/%Y'), 132, 1);
	
INSERT INTO acteur VALUES
	(4, 'Galabru','Michel', STR_TO_DATE('27/10/1922','%d/%m/%Y'), 277, 1);

INSERT INTO acteur VALUES
	(5, 'Parillaud','Anne', STR_TO_DATE('06/05/1960','%d/%m/%Y'), 35, 1);
	
INSERT INTO acteur VALUES
	(6, 'Ford','Harrison', STR_TO_DATE('13/06/1942','%d/%m/%Y'), 64, 2);
	
INSERT INTO acteur VALUES
	(7, 'Fisher','Carrie', STR_TO_DATE('21/10/1956','%d/%m/%Y'), 74, 2);
	
INSERT INTO acteur VALUES
	(8, 'Saldana','Zoé', STR_TO_DATE('19/06/1978','%d/%m/%Y'), 31, 2);
	
INSERT INTO acteur VALUES
	(9, 'Weaver','Sigourney', STR_TO_DATE('08/10/1949','%d/%m/%Y'), 66, 2);

INSERT INTO acteur VALUES
	(10, 'Reno','Jean', STR_TO_DATE('30/06/1948','%d/%m/%Y'), 75, 1);
	
INSERT INTO acteur VALUES
	(11, 'Boon','Danny', STR_TO_DATE('26/06/1966','%d/%m/%Y'), 23, 1);
	
INSERT INTO acteur VALUES
	(12, 'Merad','Kad', STR_TO_DATE('27/03/1964','%d/%m/%Y'), 55, 3);
	


	
	

	

	

	
	
	
