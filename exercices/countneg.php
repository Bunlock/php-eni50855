<html>
	<head>
		<title>CountNeg</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="bootstrap.min.css" />
		<script src="bootstrap.min.js"></script>
	</head>
	<body>
	<?php
		$serie = '1, 3, -5, -8, 4';
	?>
	
		<form action="countneg.php" method="POST">
			<input name="serie" value="<?php 
			// on vient insérer notre série dans le value pour éviter d'effacer la saisie au moment de la validation du formulaire
			echo $serie 
			?>">
			<input type="submit">
		</form>
	<?php 
		//d'abord, on vérifie qu'on a bien cliqué sur le bouton // gestion du premier chargement de la page
		if (isset($_POST['serie']))  {
			$serie = $_POST['serie']; 
			countNbNeg($serie);
		}
		
		function countNbNeg($serie){
			//on transforme notre chaine en un tableau en fonction du délimiteur virgule
			$serie = explode(',',$serie);
			//$serie = split(',\s|,',$serie); //autre méthode, dépréciée
			//print_r($serie);
			
			//on transtype chaque string du tableau construit juste avant en type entier
			for($i = 0; $i < sizeof($serie); $i++) settype($serie[$i],'int');
			
			$compteur = 0;
			foreach($serie as $n) if ($n < 0) $compteur++;
			echo $compteur .' nb negs';
			//echo '<br/>'.$n; //.' : '.gettype($n).' ';
		}
		
	?>
	</body>
</html>