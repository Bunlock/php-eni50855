<html>
	<head>
		<title>Damiers</title>
		<style>
			table {
				cellspacing: 0; cellpadding: 0;border-spacing : 0; 
				border-collapse : collapse; border: 1px solid black;}
			td {width: 15px; height: 15px; }
			.black{ background: black;}
		</style>
	</head>
	<body>
		<?php 
			function motif($i, $j){
				if (!isset($_GET['motif'])) $motif = 'dam';
				else $motif = $_GET['motif'];
				if ($motif == 'dam') if (($i+$j)%2 == 0) return '<td class="black">&nbsp;</td>'; else return '<td>&nbsp;</td>';
				if ($motif == 'hor') if (($i)%2 == 0) return '<td class="black">&nbsp;</td>'; else return '<td>&nbsp;</td>';
				if ($motif == 'vert') if (($j)%2 == 0) return '<td class="black">&nbsp;</td>'; else return '<td>&nbsp;</td>';	
			}
			
			function drawTable(){
				if (!isset($_GET['dim'])) $dim = 10 ; else $dim = $_GET['dim'];
				$flow = '<table>';
				for ($i = 0; $i < $dim; $i++){
					$flow.= '<tr>';
					for ($j = 0; $j < $dim; $j++) $flow.= motif($i, $j);
					$flow.= '</tr>';
				}
				$flow .= '</table>';
				return $flow;
			}
		?>
		<form method="GET" action="damiers2.php">
			<input type="number" name="dim" value="10">
			<select name="motif">
				<option value="dam">Damiers</option>
				<option value="hor">Horizontal</option>
				<option value="vert">Vertical</option>
			</select>
			<input type="submit" value="GO">
		</form>
		<?php echo drawTable(); ?>
		<br/>
	</body>
</html>