<?php 
// On définit un login et un mot de passe de base pour tester notre exemple. 
//Cependant, vous pouvez très bien interroger votre base de données afin de savoir 
// si le visiteur qui se connecte est bien membre de votre site
$login_valide = "moi";
$pwd_valide = "lemien";

// on teste si nos variables sont définies
//if (isset($_POST['login']) && $_POST['login'] != null && $_POST['pwd'] != null && isset($_POST['pwd'])) {
if (!empty($_POST['login']) && !empty($_POST['pwd'])){
	// on vérifie les informations du formulaire,
	//	à savoir si le pseudo saisi est bien un pseudo autorisé, de même pour le mot de passe
	if ($login_valide == $_POST['login'] && $pwd_valide == $_POST['pwd']) {
		// dans ce cas, tout est ok, on peut démarrer notre session

		// on la démarre :)
		session_start();
		// on enregistre les paramètres de notre visiteur comme variables de session ($login et $pwd)
		//(notez bien que l'on utilise pas le $ pour enregistrer ces variables)
		$_SESSION['login'] = $_POST['login'];
		$_SESSION['pwd'] = $_POST['pwd'];

		// on redirige notre visiteur vers une page de notre section membre
		header('location: roll_dice.php');
	}
	else {
		// Le visiteur n'a pas été reconnu comme étant membre de notre site. 
		// puis on le redirige vers la page d'accueil
		//echo '<script>alert("boom");</script>';
		echo '<body onLoad="alert(\'Membre non reconnu...\')">';
		
		echo '<meta http-equiv="refresh" content="0;URL=login.html">';
		//header('location: login.html');
		
		
	}
}
else {
	echo 'Il faut saisir son login et son mot de passe.<a href="login.html">Retry</a>';
}

?>